
# versatile

## A theme for Hugo.

The theme is *versatile* because it allows a site to be deployed

- with or without a web-server and
- with LaTeX-style rendering, even with no access to the internet.

The theme has been tested both for deployment on a filesystem when there is not
internet-access and via a web-server out on the internet.

Careful attention has been paid to the construction of the templates so that
they enable filesystem-usage without a web-server and ordinary usage via a
web-server with the same `config.toml`.

To enable the rendering of mathematics even when there is no access to the
internet, a version of [KaTeX](https://katex.org) is integrated into the theme
via `git-lfs`.

The `title:`, `image:`, and `description:` fields in the front-matter of a post
or a page are copied, respectively into the `og:title`, `og:image`, and
`og:description` meta-properties of the HTML head.

- By providing these properties, the theme makes pasting a link into Facebook,
  or into other social media, produce an optimal result in that medium.

- As of this writing, Facebook recomments keeping the image's aspect ratio near
  1.91:1 in order to avoid cropping.

- Also, the `image:` field in the front-matter needs to specify the path to the
  image relative to the directory (most likely `posts`) in which the markdown
  for the corresponding post resides.
    - I don't know why that is, but doing it that way seems required for
      Facebook to use the image.
    - One consequence of this requirement is that I cannot easily include the
      image in the summary.  So I gave up on that idea.

### Configuration

For file-system-only (no-web-browser) mode, these three settings in the
Hugo-based site's `config.toml` must hold:

1. `relativeURLs = "true"`
2. `uglyURLs = "true"`
3. Either
    a. `baseURL = "/"`, or
    b. Do not set `baseURL`, but do set `canonifyURLs = "true"`.

Also in `config.toml`, parameters used by theme 'versatile':

```
[params]
dateFormat = "2006 Jan 02"  # Or whatever you want.
subtitle = "My Blog's Subtitle"
# The 'author' data goes into the 'author' meta-tag in the HTML head.
author = "My Name"
email = "foo@somewhere.com"
copyright = "My Name or My Company"
```

### Example

[My blog](https://tevaughan.gitlab.io/TooLittleTooLate/) uses `versatile`.  I
have verified that my blog works both with and without a web-server.

Also, see [my config.toml][cfg].

[cfg]: https://gitlab.com/tevaughan/TooLittleTooLate/-/blob/master/config.toml

