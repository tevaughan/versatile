---
title: "{{replace .Name "-" " " | replaceRE " [0-9][0-9][0-9][0-9].*" "" | title}}"
date: {{ .Date }}
description: ""
image: ""
tags: []
---

<!--more-->

<!--
  text before 'more': appears in summary and on article's page
  text after  'more': appears only on article's page
-->
